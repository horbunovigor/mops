(function ($) {
  ("use strict");
  // variables
  var layout = $(".layout"),
    header = $(".layout__header");
  // preloader
  preloader();
  function preloader() {
    layout.on("click", ".nav__link", function (event) {
      layout.removeClass("layout_ready-load");
      event.preventDefault();
      var linkLocation = this.href;
      setTimeout(function () {
        window.location = linkLocation;
      }, 500);
    });
    setTimeout(function () {
      layout.addClass("layout_ready-load");
    }, 0);
  }

  if ($(".slider").length) {
    $(".slider").find(".slider__list").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 1000,
      fade: true,
      dots: true,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 41"  fill="none"><path d="M19 2.33333L3 20.3333L19 38.3333"  stroke-width="4" stroke-linecap="round"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 40"  fill="none"><path d="M2 2L18 20L2 38"  stroke-width="4" stroke-linecap="round"/></svg></div>',
    });
  }

  if ($(".certificates__list").length) {
    $(".certificates__list").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      variableWidth: true,
      speed: 1000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18 32" fill="none"><path d="M15.5 2.5L3.5 16L15.5 29.5" stroke="#B1A08D" stroke-width="4" stroke-linecap="round"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 40"  fill="none"><path d="M2 2L18 20L2 38"  stroke-width="4" stroke-linecap="round"/></svg></div>',
    });
  }

  if ($(".products__list").length) {
    $(window).on("load resize", function () {
      if ($(window).width() > 720) {
        $(".products__list").each(function () {
          $(this).slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: false,
            arrows: true,
            speed: 1000,
            prevArrow:
              '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 18 32" fill="none"><path d="M15.5 2.5L3.5 16L15.5 29.5" stroke="#B1A08D" stroke-width="4" stroke-linecap="round"/></svg></div>',
            nextArrow:
              '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 40"  fill="none"><path d="M2 2L18 20L2 38"  stroke-width="4" stroke-linecap="round"/></svg></div>',
          });
        });
      } else if ($(".products__list").hasClass("slick-initialized")) {
        $(".products__list").slick("unslick");
      }
    });
  }

  if ($(".team__slider").length) {
    $(".team__slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 1000,
      fade: true,
      dots: true,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 40"  fill="none"><path d="M19 2L3 20L19 38"  stroke-width="4" stroke-linecap="round"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 21 40"  fill="none"><path d="M2 2L18 20L2 38"  stroke-width="4" stroke-linecap="round"/></svg></div>',
    });
  }

  // Menu
  navInit();
  function navInit() {
    header.find(".header__burger").on("click", function () {
      $(this).closest(header).toggleClass("layout__header_menu-active");
    });
  }

  $(document).mouseup(function (e) {
    if ($(".layout__header_menu-active").length) {
      var div = $(".header__primary");
      if (!div.is(e.target) && div.has(e.target).length === 0) {
        header.removeClass("layout__header_menu-active");
      }
    }
  });

  $(window).keydown(function (e) {
    if (e.key === "Escape") {
      header.removeClass("layout__header_menu-active");
    }
  });

  /// Scroll functions
  $(window).on("load resize scroll", function () {
    let h = $(window).height();
    scrollHeader(h);
    scrollSection(h);
    scrollImage(h);
  });
  function scrollHeader(h) {
    if ($(window).scrollTop() >= 1) {
      header.addClass("site-header_animation");
    } else {
      header.removeClass("site-header_animation");
    }
  }
  function scrollSection(h) {
    let section = $(".section");
    section.each(function () {
      if ($(window).scrollTop() + h >= $(this).offset().top) {
        $(this).addClass("section_animation");
      }
    });
  }
  function scrollImage(h) {
    // Image initialization
    let img = $("img");
    img.each(function () {
      if (
        $(window).scrollTop() + h >= $(this).offset().top &&
        this.getAttribute("data-src") &&
        this.src !== this.getAttribute("data-src")
      ) {
        this.src = this.getAttribute("data-src");
      }
    });
  }

  // Tabs init
  if ($(".tabs").length) {
    tabsInit();
  }

  function tabsInit() {
    let position,
      tabsBodyItem = $(".tabs__body").find(".tabs__item"),
      tabsActive = "tabs__item_active";
    $(".tabs__header").on("click", ".tabs__item", function () {
      position = $(this).index();
      $(this).addClass(tabsActive).siblings().removeClass(tabsActive);
      tabsBodyItem
        .eq(position)
        .addClass(tabsActive)
        .siblings()
        .removeClass(tabsActive);
    });
    $(".tabs").on("click", ".tabs__header", function () {
      $(this).toggleClass("tabs__header_active");
    });
  }

  // Accordion init
  if ($(".accordion").length) {
    accordionInit();
  }

  function accordionInit() {
    $(".accordion").on("click", ".accordion__header", function () {
      $(this)
        .closest(".accordion__item")
        .toggleClass("accordion__item_active")
        .siblings()
        .removeClass("accordion__item_active");
    });
  }

  // Validation & customize form
  if ($("form").length) {
    // validation init
    formValidation();
    // select init
    jcf.setOptions("Select", {
      wrapNative: false,
      wrapNativeOnMobile: false,
      fakeDropInBody: false,
      maxVisibleItems: 5,
    });
    // Number init
    jcf.setOptions("Number", {
      fakeStructure:
        '<span class="jcf-number"><span class="jcf-btn-dec"><svg width="10" height="2" viewBox="0 0 10 2"  xmlns="http://www.w3.org/2000/svg"> <path d="M1 1L9 1" stroke="#006B54" stroke-width="2" stroke-linecap="round"/></svg></span><span class="jcf-btn-inc"><svg width="10" height="10" viewBox="0 0 10 10"  xmlns="http://www.w3.org/2000/svg"><path d="M5 1L5 9M9 5L1 5" stroke="#006B54" stroke-width="2" stroke-linecap="round"/></svg></span></span>',
    });
    jcf.replaceAll();
    // Mask form
    $(".phone_mask").mask("8(999) 999-99-99");

    // Attach
    if ($(".attach").length) {
      attach();
    }
    function attach() {
      let attach = $(".attach"),
        attachList = attach.find(".attach__list"),
        attachItemEmpty = $(".attach").find(".attach__list").html();
      attach.find(".attach__list").html("");

      // init
      attach.on("click", ".attach__init", function () {
        attachList.append(attachItemEmpty);
        $(this)
          .closest(".attach")
          .find(attachList)
          .find(".attach__item")
          .last()
          .hide();
        $(this)
          .closest(".attach")
          .find(attachList)
          .find(".attach__item")
          .last()
          .find(".attach__input")
          .find(".input")
          .click();
      });
      // change
      attach.on("change", ".input", function () {
        $(this).closest(".attach__item").show();
        $(this)
          .closest(".attach__item")
          .find(".attach__title")
          .find(".title__text")
          .text($(this).prop("files")[0].name);
      });
      // remove
      attach.on("click", ".attach__action", function () {
        $(this).closest(".attach__item").remove();
      });
    }

    /* Slider */
    if ($(".range").length) {
      rangeInit();
    }
    function rangeInit() {
      $(".range__slider").slider({
        range: true,
        min: 1500,
        max: 10000,
        step: 100,
        values: [3000, 6000],
        slide: function (event, ui) {
          console.log(ui.values[0]);
          $(".range__input_min").val(ui.values[0]);
          $(".range__input_max").val(ui.values[1]);
        },
      });
      $(".range__input_min").val($(".range__slider").slider("values", 0));
      $(".range__input_max").val($(".range__slider").slider("values", 1));
    }

    /* Calendar */
    if ($(".datepicker").length) {
      datepickerInit();
    }
    function datepickerInit() {
      /* Датапикер */
      $.datepicker.regional["ru"] = {
        closeText: "Закрыть",
        currentText: "Сегодня",
        prevText:
          '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" ><path d="M13 4L7 10L13 16" stroke="#7A828A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        nextText:
          '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" ><path d="M7 16L13 10L7 4" stroke="#7A828A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>',
        monthNames: [
          "Январь",
          "Февраль",
          "Март",
          "Апрель",
          "Май",
          "Июнь",
          "Июль",
          "Август",
          "Сентябрь",
          "Октябрь",
          "Ноябрь",
          "Декабрь",
        ],
        monthNamesShort: [
          "Янв",
          "Фев",
          "Мар",
          "Апр",
          "Май",
          "Июн",
          "Июл",
          "Авг",
          "Сен",
          "Окт",
          "Ноя",
          "Дек",
        ],
        dayNames: [
          "воскресенье",
          "понедельник",
          "вторник",
          "среда",
          "четверг",
          "пятница",
          "суббота",
        ],
        dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        weekHeader: "Не",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: "",
      };
      $.datepicker.setDefaults($.datepicker.regional["ru"]);
      /* Датапикер Одиночный*/
      $(".datepicker").datepicker({
        showOtherMonths: true,
        minDate: 0,
      });

      /* Датапикер период*/
      $(".datepicker_range").datepicker({
        onSelect: function (selectedDate) {
          if (!$(this).data().datepicker.first) {
            $(this).data().datepicker.inline = true;
            $(this).data().datepicker.first = selectedDate;
          } else {
            if (selectedDate > $(this).data().datepicker.first) {
              $(this).val(
                $(this).data().datepicker.first + " - " + selectedDate
              );
            } else {
              $(this).val(
                selectedDate + " - " + $(this).data().datepicker.first
              );
            }
            $(this).data().datepicker.inline = false;
          }
        },
        onClose: function () {
          delete $(this).data().datepicker.first;
          $(this).data().datepicker.inline = false;
        },
      });
    }
  }

  function formValidation() {
    let form = $("form");
    form.submit(function () {
      if ($(this).valid()) {
        return true;
      } else {
        return false;
      }
    });
    form.validate({
      rules: {
        name: {
          required: true,
          name: true,
        },
        phone: {
          required: true,
        },
      },
    });
  }

  // MODAL INIT
  modalInit();
  function modalInit() {
    let modalName;
    // modal show
    $(document).on("click", ".modal-init", function () {
      layout
        .addClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
      modalName = $(this).data("modalname");
      layout.find("." + modalName + "").addClass("modal__layout_active");
    });
    // modal hide
    $(document).mouseup(function (e) {
      if ($(".modal__layout.active").length) {
        var div = $(".modal__layout");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
          modalHide();
        }
      }
    });
    // modal hide
    $(document).on("click", ".modal__action", function () {
      modalHide();
    });
    // modal hide
    $(window).keydown(function (e) {
      if (e.key === "Escape") {
        modalHide();
      }
    });

    function modalHide() {
      layout
        .removeClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
    }
  }

  // Scroll
  linkScroll();
  function linkScroll() {
    $('a[href^="#"]:not([href="#"])').click(function (e) {
      e.preventDefault();
      var target = $($(this).attr("href"));
      if (target.length) {
        var scrollTo = target.offset().top;
        $("body, html").animate({ scrollTop: scrollTo + "px" }, 800);
      }
    });
  }
})(jQuery);
